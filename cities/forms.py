from django import forms
from .models import Cities

class CitiesForm(forms.ModelForm):
    class Meta:
        model = Cities
        fields = "__all__"
        widgets = {
            "city_name": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Insert the name of the city"}),
        }

class UpdateCitiesForm(forms.ModelForm):
    class Meta:
        model = Cities
        fields = "__all__"
        widgets = {
            "city_name": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Insert the name of the city"}),
        }