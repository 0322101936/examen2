from django.shortcuts import render
from django.views import generic
from django.urls import reverse_lazy
from .models import Cities
from .forms import CitiesForm, UpdateCitiesForm



# Create your views here.
#Create
class AddCity(generic.CreateView):
    template_name = "cities/city_create.html"
    model = Cities
    form_class = CitiesForm
    success_url = reverse_lazy("cities:city_list")

#List
class ListCities(generic.View):
    template_name = "cities/city_list.html"
    context = {}

    def get(self, request, *args, **kwargs):
        cities_list = Cities.objects.all()
        self.context = {
            "cities": cities_list
        }

        return render(request, self.template_name, self.context)

#Details
class DetailCity(generic.DetailView):
    template_name = "cities/city_detail.html"
    model = Cities

#Update
class UpdateCity(generic.UpdateView):
    template_name = "cities/city_update.html"
    model = Cities
    form_class = UpdateCitiesForm
    success_url = reverse_lazy("cities:city_list")

#Delete
class DeleteCity(generic.DeleteView):
    template_name = "cities/city_delete.html"
    model = Cities
    success_url = reverse_lazy("cities:city_list")

