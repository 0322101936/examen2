from django.urls import path
from cities import views

app_name = "cities"

urlpatterns = [
    path('list/', views.ListCities.as_view(), name="city_list"),
    path('add/', views.AddCity.as_view(), name="city_add"),
    path('detail/<int:pk>/', views.DetailCity.as_view(), name="city_detail"),
    path('update/<int:pk>/', views.UpdateCity.as_view(), name="city_update"),
    path('delete/<int:pk>/', views.DeleteCity.as_view(), name="city_delete"),
]