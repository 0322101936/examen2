from django.db import models

# Create your models here.
class Cities(models.Model):
    city_name = models.CharField(max_length=255)

    def __str__(self):
        return self.city_name