from django.shortcuts import render
from django.views import generic
from django.urls import reverse_lazy
from .models import Teams
from .forms import TeamForm, UpdateTeamForm



# Create your views here.
#Create
class AddTeam(generic.CreateView):
    template_name = "teams/team_create.html"
    model = Teams
    form_class = TeamForm
    success_url = reverse_lazy("teams:team_list")

#List
class TeamList(generic.View):
    template_name = "teams/team_list.html"
    context = {}

    def get(self, request, *args, **kwargs):
        teams_list = Teams.objects.all()
        self.context = {
            "teams": teams_list
        }

        return render(request, self.template_name, self.context)

#Details
class DetailTeam(generic.DetailView):
    template_name = "teams/team_detail.html"
    model = Teams

#Update
class UpdateTeam(generic.UpdateView):
    template_name = "teams/team_update.html"
    model = Teams
    form_class = UpdateTeamForm
    success_url = reverse_lazy("teams:team_list")

#Delete
class DeleteTeam(generic.DeleteView):
    template_name = "teams/team_delete.html"
    model = Teams
    success_url = reverse_lazy("teams:team_list")

