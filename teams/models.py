from django.db import models
from cities.models import Cities
from stadiums.models import Stadiums

CONFERENCE = (
    ('American Football Conference', 'American Football Conference'),
    ('National Football Conference', 'National Football Conference'),
)

# Create your models here.
class TeamStadium(models.Model):
    team = models.ForeignKey('Teams', on_delete=models.CASCADE)
    stadium = models.ForeignKey(Stadiums, on_delete=models.CASCADE)

class Teams(models.Model):
    team_name = models.CharField(max_length=255)
    team_founded = models.IntegerField()
    team_conference = models.CharField(max_length=255, choices=CONFERENCE)
    team_city = models.ForeignKey(Cities, on_delete=models.CASCADE)
    team_stadium = models.ManyToManyField(Stadiums, through='TeamStadium')

    def __str__(self):
        return self.team_name