from django.urls import path
from teams import views

app_name = "teams"

urlpatterns = [
    path('list/', views.TeamList.as_view(), name="team_list"),
    path('add/', views.AddTeam.as_view(), name="team_add"),
    path('detail/<int:pk>/', views.DetailTeam.as_view(), name="team_detail"),
    path('update/<int:pk>/', views.UpdateTeam.as_view(), name="team_update"),
    path('delete/<int:pk>/', views.DeleteTeam.as_view(), name="team_delete"),
]