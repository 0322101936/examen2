from django import forms
from .models import Teams

class TeamForm(forms.ModelForm):
    class Meta:
        model = Teams
        fields = "__all__"
        widgets = {
            "team_name": forms.TextInput(
                attrs={
                    "type":"text", "class":"form-control", "placeholder":"Insert the name of the stadium"
                }
            ),
            "team_founded": forms.NumberInput(
                attrs={
                    "type":"number", "class":"form-control"
                }
            ),
            "team_conference": forms.Select(
                attrs={
                    "type":"select", "class":"form-control"
                }
            ),
            "team_city": forms.Select(
                attrs={
                    "type":"select", "class":"form-control"
                }
            ),
            "team_stadium": forms.SelectMultiple(
                attrs={
                    "type":"select", "class":"form-control"
                }
            )
        }

class UpdateTeamForm(forms.ModelForm):
    class Meta:
        model = Teams
        fields = "__all__"
        widgets = {
            "team_name": forms.TextInput(
                attrs={
                    "type":"text", "class":"form-control", "placeholder":"Insert the name of the stadium"
                }
            ),
            "team_founded": forms.NumberInput(
                attrs={
                    "type":"number", "class":"form-control"
                }
            ),
            "team_conference": forms.Select(
                attrs={
                    "type":"select", "class":"form-control"
                }
            ),
            "team_city": forms.Select(
                attrs={
                    "type":"select", "class":"form-control"
                }
            ),
            "team_stadium": forms.SelectMultiple(
                attrs={
                    "type":"select", "class":"form-control"
                }
            )
        }