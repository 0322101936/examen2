from django.urls import path
from stadiums import views

app_name = "stadiums"

urlpatterns = [
    path('list/', views.StadiumList.as_view(), name="stadium_list"),
    path('add/', views.AddStadium.as_view(), name="stadium_add"),
    path('detail/<int:pk>/', views.DetailStadium.as_view(), name="stadium_detail"),
    path('update/<int:pk>/', views.UpdateStadium.as_view(), name="stadium_update"),
    path('delete/<int:pk>/', views.DeleteStadium.as_view(), name="stadium_delete"),
]