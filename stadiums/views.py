from django.shortcuts import render
from django.views import generic
from django.urls import reverse_lazy
from .models import Stadiums
from .forms import StadiumForm, UpdateStadiumForm



# Create your views here.
#Create
class AddStadium(generic.CreateView):
    template_name = "stadiums/stadium_create.html"
    model = Stadiums
    form_class = StadiumForm
    success_url = reverse_lazy("stadiums:stadium_list")

#List
class StadiumList(generic.View):
    template_name = "stadiums/stadium_list.html"
    context = {}

    def get(self, request, *args, **kwargs):
        stadium_list = Stadiums.objects.all()
        self.context = {
            "stadium": stadium_list
        }

        return render(request, self.template_name, self.context)

#Details
class DetailStadium(generic.DetailView):
    template_name = "stadiums/stadium_detail.html"
    model = Stadiums

#Update
class UpdateStadium(generic.UpdateView):
    template_name = "stadiums/stadium_update.html"
    model = Stadiums
    form_class = UpdateStadiumForm
    success_url = reverse_lazy("stadiums:stadium_list")

#Delete
class DeleteStadium(generic.DeleteView):
    template_name = "stadiums/stadium_delete.html"
    model = Stadiums
    success_url = reverse_lazy("stadiums:stadiums_list")

