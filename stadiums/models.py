from django.db import models
from cities.models import Cities

# Create your models here.
class Stadiums(models.Model):
    stadium_name = models.CharField(max_length=255)
    stadium_capacity = models.IntegerField()
    stadium_city = models.ForeignKey(Cities, on_delete=models.CASCADE)

    def __str__(self):
        return self.stadium_name