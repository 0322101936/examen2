from django import forms
from .models import Stadiums

class StadiumForm(forms.ModelForm):
    class Meta:
        model = Stadiums
        fields = "__all__"
        widgets = {
            "stadium_name": forms.TextInput(
                attrs={
                    "type":"text", "class":"form-control", "placeholder":"Insert the name of the stadium"
                }
            ),
            "stadium_capacity": forms.NumberInput(
                attrs={
                    "type":"number", "class":"form-control"
                }
            ),
            "stadium_city": forms.Select(
                attrs={
                    "type":"select", "class":"form-control"
                }
            )
        }

class UpdateStadiumForm(forms.ModelForm):
    class Meta:
        model = Stadiums
        fields = "__all__"
        widgets = {
            "stadium_name": forms.TextInput(
                attrs={
                    "type":"text", "class":"form-control", "placeholder":"Insert the name of the stadium"
                }
            ),
            "stadium_capacity": forms.NumberInput(
                attrs={
                    "type":"number", "class":"form-control"
                }
            ),
            "stadium_city": forms.Select(
                attrs={
                    "type":"select", "class":"form-control"
                }
            )
        }